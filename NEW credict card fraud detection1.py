import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)


#read dataset
data=pd.read_csv('C:/Users/Navpreet/Desktop/creditcard.csv')
RANDOM_SEED = 20
data.head(10)


#check null values
data.isnull().sum()


#determine dependant and independant variables
first=data.drop(['Class'],axis=1)
first


second=data['Class']
second


first.shape


second.shape


#check imbalalencing
LABELS=['Normal','Fraud']
import matplotlib.pyplot as plt
count_classes = pd.value_counts(data['Class'], sort = True)
count_classes.plot(kind = 'bar', rot=0)
plt.title("Transaction Class Distribution")
plt.xticks(range(2), LABELS)
plt.xlabel("Class")
plt.ylabel("Frequency")


#from above graph we see that data imbalencing is much more so covert them into balenced dataset
#to check it in values use following piece of code

fraud_data = data[data['Class']==1]
normal_data = data[data['Class']==0]
print(fraud_data.shape,normal_data.shape)



from imblearn.combine import SMOTETomek
from imblearn.under_sampling import NearMiss


# Implementing Oversampling for Handling Imbalanced 
smk = SMOTETomek(random_state=42)
first_res,second_res=smk.fit_sample(first,second)


first_res.shape,second_res.shape


from collections import Counter
print('Original dataset shape {}'.format(Counter(second)))
print('Resampled dataset shape {}'.format(Counter(second_res)))


count_val=pd.value_counts(second_res)
count_val.plot(kind='bar',rot=0)


from sklearn.model_selection import train_test_split
first_train,first_test,second_train,second_test=train_test_split(first_res,second_res,random_state=0,test_size=0.20)


from sklearn.linear_model import LogisticRegression
regress=LogisticRegression(max_iter=120000)
regress


regress.fit(first_train,second_train)


print(first_test)


print(second_test)


second_pred=regress.predict(first_test)
print(second_pred)


from sklearn.metrics import classification_report,accuracy_score
print(classification_report(second_test,second_pred))


print(accuracy_score(second_test,second_pred))
